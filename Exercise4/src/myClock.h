#ifndef MY_CLOCK
#define MY_CLOCK

#include "CGF/CGFobject.h"
#include "myCylinder.h"
#include "myUnitCube.h"
#include "MyClockHand.h"

class myClock : public CGFobject {

	private:
		myCylinder* cilindro;

	public:
		myClock();
		void draw();
};



#endif
