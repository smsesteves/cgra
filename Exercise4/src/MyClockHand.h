#ifndef MYCLOCKHAND_H
#define MYCLOCKHAND_H

#include "CGFobject.h"
#include "myUnitCube.h"

class myClockHand: public CGFobject {

	myUnitCube* ponteiro;
	public:
		myClockHand();
		void draw();
};

#endif