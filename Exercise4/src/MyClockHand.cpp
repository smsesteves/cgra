#include "MyClockHand.h"

myClockHand::myClockHand()
{
	ponteiro = new myUnitCube();
}

void myClockHand::draw()
{
	glPushMatrix();
	glTranslated(0,0.5,0);
	glScaled(0.2,1,0.2);
	
	//glScaled(1,3,1);
	//glScaled(0.05,0.2,0.7);
	//glTranslated(0,0.5,0.5);
	ponteiro->draw();
	glPopMatrix();
}